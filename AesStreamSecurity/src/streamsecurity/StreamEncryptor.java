/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package streamsecurity;

import java.io.OutputStream;
import java.security.spec.KeySpec;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author vaganovdv
 */
public class StreamEncryptor
{

       private static String salt      = "12345"; 
    
    public StreamEncryptor()
    {
    }

    
     public CipherOutputStream encrypt(OutputStream out, String secret)
    {
        try
        {
            
            byte[] iv =
            {
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
            };
            IvParameterSpec ivspec = new IvParameterSpec(iv);

            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
            
            // Фомрирование 256 байтного секретного ключа на основе пароля + salt
            //                                  пароль   дополнительный пароль (salt)    к-во итераций алгоритма     длина ключа шифрования
            //                                   |                  |                       |                           |
            KeySpec spec = new PBEKeySpec(secret.toCharArray(), salt.getBytes(),           65536,                      256);
            SecretKey tmp = factory.generateSecret(spec);
            SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");

            //  Cipher Block Chaining (CBC)
            //  Режим сцепления блоков шифротекста
            
            
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivspec);
            
            CipherOutputStream cipherOut = new CipherOutputStream(out, cipher);
            
            return cipherOut;
            
        } catch (Exception e)
        {
            System.out.println("Error while encrypting: " + e.toString());
        }
        return null;
    }
    
    
    
}
