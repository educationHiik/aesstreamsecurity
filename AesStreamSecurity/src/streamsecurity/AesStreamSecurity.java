/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package streamsecurity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;

/**
 *
 * @author vaganovdv
 */
public class AesStreamSecurity
{

   
    
    public static void main(String[] args)
    {
       
        String secretKey = "123";
       
       
       
        File openfile   = new File("open.txt");
        File securefile = new File("secure.bin");
        FileOutputStream output = null;
        FileInputStream input  = null;
         PrintWriter writer;
        
         
       StreamDecryptor streamDecryptor = new StreamDecryptor();
       StreamEncryptor StreamEncryptor = new StreamEncryptor();
        
        try
        {
            if (openfile.createNewFile())
            {
                System.out.println("Создан файл с октрытым тесктом:     {" + openfile.getAbsolutePath() + "");
                output = new FileOutputStream(openfile.getAbsolutePath());
                writer = new PrintWriter(output);

                for (int i = 0; i < 20; i++)
                {
                    writer.println("Очень секретное сообщение "+i);
                }
                writer.flush();
                writer.close();
            }
            
            if (securefile.createNewFile())
            {
                System.out.println("Создан файл с шированным текстом:   {" + securefile.getAbsolutePath() + "");
                output = new FileOutputStream(securefile.getAbsolutePath());
                CipherOutputStream secureout = StreamEncryptor.encrypt(output, secretKey);
                writer = new PrintWriter(secureout);
                for (int i = 0; i < 20; i++)
                {
                    writer.println("Очень секретное сообщение "+i);
                }
                
                writer.flush();
                writer.close();
                input = new FileInputStream(securefile.getAbsolutePath());

            }

            CipherInputStream cipherIn = streamDecryptor.decrypt(input, secretKey);
            Scanner sc = new Scanner(cipherIn);
           while (sc.hasNextLine())
           {
               System.out.println(sc.nextLine());
           }

          
        } catch (IOException ex)
        {
            Logger.getLogger(AesStreamSecurity.class.getName()).log(Level.SEVERE, null, ex);
        }


    }
    
}
